const path = require(`path`);

const config = {};

config.siteMetadata = {
    title: `Blue Napoleon (unofficial)`,
    description: `Blue Napoleon (unofficial)`,
    url: "https://napoleon.blue",
    assetServer: "https://napoleon.blue",
};

config.mapping = {
    'MarkdownRemark.frontmatter.author': `AuthorJson`
};

config.pathPrefix = require('./pathPrefix');

config.plugins = [
    `gatsby-plugin-typescript`,
    `gatsby-plugin-postcss-sass`,
    {
        resolve: `gatsby-source-filesystem`,
        options: {
            name: `data`,
            path: `${__dirname}/data`
        }
    },
    {
        resolve: `gatsby-transformer-remark`,
        options: {
            plugins: [
                {
                    resolve: `gatsby-remark-responsive-image`,
                    options: {
                        maxWidth: 690,
                        backgroundColor: `#f7f0eb`
                    }
                },
                `gatsby-remark-prismjs`,
                `gatsby-remark-copy-linked-files`,
                `gatsby-remark-autolink-headers`,
            ]
        }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-json`,
    `gatsby-transformer-sharp`,
    {
        resolve: `gatsby-plugin-manifest`,
        options: {
            name: `napoleon.blue (unofficial)`,
            short_name: `napoleon.blue`,
            start_url: `/`,
            background_color: `#f7f7f7`,
            theme_color: `#191919`,
            display: `minimal-ui`
        }
    },
    `gatsby-plugin-offline`,
    {
        resolve: `gatsby-plugin-google-analytics`,
        options: {
            trackingId: 'UA-88361167-3',
        },
    }
];

if (process.env.NODE_ENV === "production") {
    // TODO: ビルドした時に反映されないような気がする…
    config.plugins.push();
}

module.exports = config;
