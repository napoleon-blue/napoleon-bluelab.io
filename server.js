/**
 * Created by tottokotkd on 28/05/2017.
 */

const Express = require('express');
const app = Express();

app.use(Express.static('public'));
app.listen(3000);
console.log('http://localhost:3000');
