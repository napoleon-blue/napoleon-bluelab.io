# This file is a template, and might need editing before it works on your project.
FROM node:latest

WORKDIR /napoleon.blue

COPY . /napoleon.blue
RUN yarn install && npm run build || true && npm run build && rm -rdf .cache node_modules src static && npm install express

CMD ["npm", "run", "server"]

EXPOSE 3000
