# coding:utf-8

import colorsys
import sys
import os
import glob
import re
import cv2

directory = sys.argv[1]
if not os.path.isdir(directory):
    print 'error: directory not found'
    sys.exit(-1)

if not os.path.exists('./dist'):
    os.mkdir('./dist')

width = 16
height = 9


def to_hex(red, green, blue):
    def to_str(decimal):
        return ('0' + hex(decimal).split('x')[-1])[-2:]
    return "#%s%s%s" % (to_str(red), to_str(green), to_str(blue))

joined_output = open('output.txt', 'w')


def get_font_color(red, green, blue):
    if red == 255 & green == 255 & blue == 255:
        return 0, 0, 0

    h, l, s = colorsys.rgb_to_hls(red / 255.0, green / 255.0, blue / 255.0)

    if h >= 0.5:
        h = h - 0.5
    else:
        h = h + 0.5

    if l >= 0.5:
        l = l - 0.5
    else:
        l = l + 0.5

    r, g, b = colorsys.hls_to_rgb(h, l, s)
    return int(r * 255), int(g * 255), int(b * 255)


def numerical_sort(value):
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

for path in sorted(glob.glob(os.path.join(directory, '*.jpg')), key=numerical_sort):
    _, file_name = os.path.split(path)
    print file_name

    img = cv2.imread(path)
    img = cv2.resize(img, (width, height))

    cv2.imwrite('./dist/' + file_name, img)

    divided_output = open('dist/dv_' + file_name + '.txt', 'w')

    joined_output.write("\n&.%s {" % file_name)
    for index_y, colors in enumerate(img):
        for index_x, color in enumerate(colors):
            index = 1 + index_x + width * index_y
            blue = color[0]
            green = color[1]
            red = color[2]
            hex_str = to_hex(red, green, blue)
            c_str = "%d\t%d,%d\t%d\t%d\t%d\t%s\n" % (index, index_x, index_y, red, green, blue, hex_str)
            divided_output.write(c_str)

            f_red, f_green, f_blue = get_font_color(red, green, blue)
            joined_output.write(
                "\n  .panel:nth-child(%d) {\n    background-color: rgba(%d, %d, %d, $bc-alpha);\n    color: rgba(%d, %d, %d, $font-alpha);\n  }\n"
                % (index, red, green, blue, f_red, f_green, f_blue)
            )

    joined_output.write("}\n")
    divided_output.close()

joined_output.close()