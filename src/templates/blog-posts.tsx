/**
 * Created by tottokotkd on 2017/06/18.
 */

import * as React from "react";

import PostsList from "../components/article/List";
import {IProps as ArticleProps} from "../components/article/ListItem";
import {MenuItem} from "../layouts/content";

export default class extends React.Component<IProps, {}> {
    // publicをつけると壊れる
    // tslint:disable-next-line: member-access
    render() {
        const {posts} = this.props.data;
        if (posts) {
            return <PostsList menu={MenuItem.blog} posts={posts.edges.map((e) => e.node)} />;
        } else {
            return <div>公開記事が1つもないっス (未対応)</div>;
        }
    }
}

interface IProps {
    data: { posts?: {
        edges: [{node: ArticleProps}];
    } };
}

export const pageQuery = graphql`
query BlogPosts($skip: Int!) {
  posts: allMarkdownRemark(
    limit: 10
    skip: $skip
    sort: { fields: [frontmatter___createdDate], order: DESC }
    filter: {
      fields: {slug: { regex: "^/blog/\\d+-\\d+-\\d+_\\d+/" }}
      frontmatter: {draft: {ne: true}}
    }
  ) {
    edges {
      node {
        excerpt
        timeToRead
        fields {
          slug
        }
        frontmatter {
          title
          tags
          desc
          createdDate
          updatedDate
          icon {
            sharp: childImageSharp {
              data: responsiveResolution(width: 80, height: 80, quality: 100) {
                src
                srcSet
              }
            }
          }
          author {
            icon {
              sharp: childImageSharp {
                data: responsiveResolution(width: 80, height: 80, quality: 100) {
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
}
`;
