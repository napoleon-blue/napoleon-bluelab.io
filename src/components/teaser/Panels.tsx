/**
 * Created by tottokotkd on 27/05/2017.
 */

import Link from "gatsby-link";
import {range} from "lodash";
import * as React from "react";
import Helmet from "react-helmet";

import {assetLink, prefixLink} from "../../utils/path-helper";

import "../../css/teaser/index.scss";
import "../../css/teaser/index.scss";
import "../../css/teaser/panel_color.scss";
import "../../css/teaser/panel_transition.scss";

interface IPanelProps {
    panelState?: string;
    showButton: boolean;
    showMenu: boolean;
    onPreviousClick?: string;
    onNextClick?: string;
    onTwitterClick?: string;
    onPlayClick?: string;
    onShareClick?: string;
}
class Panels extends React.Component<IPanelProps, {}> {

    public static allNames = [
        "s1-1-1", "s1-1-2",
        "s1-2-1", "s1-2-2",
        "s1-3-1", "s1-3-2",
        "s2-1-1", "s2-1-2",
        "s2-2-1", "s2-2-2",
        "s2-3-1", "s2-3-2",
        "s3-1-1", "s3-1-2",
        "s3-2-1", "s3-2-2",
        "s3-3-1", "s3-3-2",
        "s4-1-1", "s4-1-2",
        "s4-2-1", "s4-2-2",
        "s4-3-1", "s4-3-2",
        "s4-4-1", "s4-4-2",
        "s5-1-1", "s5-1-2",
    ];

    public render() {
        const {
            panelState,
            showButton, showMenu,
            onPreviousClick, onNextClick, onTwitterClick, onPlayClick, onShareClick,
        } = this.props;
        const contentsClass = `${showButton ? "stopping" : "changing"} ${panelState || "position-a blue"}`;
        const logoOverlay = (
            <div className="logo-layer">
                <div className="logo">Blue Napoleon</div>
            </div>
        );
        const menuOverlay = (
            <div className="menu-layer">
                <div className="menu-frame">
                    <nav>
                        <ul>
                            <li><Link to="/news/">news</Link></li>
                            <li>profile</li>
                            <li><Link to="/blog/">blog</Link></li>
                            <li><Link to="/about/">about</Link></li>
                        </ul>
                    </nav>
                </div>
            </div>
        );

        return (
            <div id="teaser-content">
                <Helmet
                    meta={[
                        {name: "twitter:card", content: "summary_large_image"},
                        {name: "twitter:image", content:  assetLink("/img/twitter_card.png")},
                    ]}>
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster" async={false} />
                </Helmet>
                <div id="page-box">
                    <div className="panels-layer">
                        <div id="panel-content" className={contentsClass} >
                            { range(0, 16 * 9).map((index) => {

                                let linkTo: string = "";
                                if (showButton) {
                                    switch (index) {
                                        case 70:
                                            if (onPreviousClick) {
                                                linkTo = onPreviousClick;
                                            }
                                            break;
                                        case 71:
                                            if (onTwitterClick) {
                                                linkTo = onTwitterClick;
                                            }
                                            break;
                                        case 72:
                                            if (onPlayClick) {
                                                linkTo = onPlayClick;
                                            }
                                            break;
                                        case 73:
                                            if (onNextClick) {
                                                linkTo = onNextClick;
                                            }
                                            break;
                                    }
                                } else {
                                    switch (index) {
                                        case 7:
                                            if (onShareClick) {
                                                linkTo = onShareClick;
                                            }
                                            break;
                                    }
                                }

                                if (linkTo.match(/https?:\/\/.*/)) {
                                    return <div key={index} onClick={() => location.href = linkTo} className="panel"/>;
                                } else if (linkTo) {
                                    return <Link key={index} to={linkTo} className="panel"/>;
                                } else {

                                    return <div key={index} className="panel" />;
                                }
                            }) }
                        </div>
                    </div>
                    <div className="overlay-box">
                        { !showButton ? logoOverlay : null }
                        { showMenu ? menuOverlay : null }
                    </div>
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default Panels;
