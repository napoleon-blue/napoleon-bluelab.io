/**
 * Created by tottokotkd on 28/05/2017.
 */
import {find, get} from "lodash";
import * as React from "react";
import Helmet from "react-helmet";
import * as ReactRouter from "react-router";

import * as config from "../../../gatsby-config";
import {assetLink, prefixLink} from "../../utils/path-helper";
import Panels from "./Panels";

interface IProps extends ReactRouter.RouteComponentProps<undefined, undefined> {
    target: string;
}

export default class StoppedPanels extends React.Component<IProps, {style: string}> {

    public state = {style: "position-a wave"};

    public componentDidMount() {
        this.setState({style: "position-a wave"});
        setTimeout(() => this.setState({style: "position-b wave " + this.props.target}), 100);
    }

    public render() {
        const {target} = this.props;
        // tscがエラー吐いてやかましいのでとりあえずany
        const c: any = config;
        const url = encodeURIComponent(`${c.siteMetadata.url}/teaser/${target}/`);
        const title = encodeURIComponent(c.siteMetadata.title);
        const hashtags = ["ブルナポ"];
        const tweetURL = `http://twitter.com/share?url=${url}&text=${title}&hashtags=${hashtags}`;
        return (
            <Panels
                panelState={this.state.style}
                showButton={true}
                showMenu={true}
                onPreviousClick={ `/teaser/${this.getPrev()}/`}
                onNextClick={`/teaser/${this.getNext()}/`}
                onTwitterClick={tweetURL}
                onPlayClick={`/`}
            >
                <Helmet meta={[{name: "twitter:image", content: assetLink(`/teaser/sn/${target}.png`) }]}/>
                {this.props.children}
            </Panels>
        );
    }

    private getPrev = () => {
        const current = Panels.allNames.indexOf(this.props.target);
        const prev = current < 1 ? Panels.allNames.length - 1 : current - 1;
        return Panels.allNames[prev];
    }

    private getNext = () => {
        const current = Panels.allNames.indexOf(this.props.target);
        const next =  current >= Panels.allNames.length - 1 ? 0 : current + 1;
        return Panels.allNames[next];
    }

    private push = (link: string) => {
        const {history} = this.props;
        if (history) {
            history.push(link);
        }
    }
}
