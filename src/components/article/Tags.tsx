/**
 * Created by tottokotkd on 2017/07/14.
 */

import {kebabCase} from "lodash";
import * as React from "react";

import Link from "gatsby-link";

import {tagLink} from "../../utils/path-helper";
import PageRow from "../Row";

export interface IProps {
    tags: string[];
}

export default class extends React.Component<IProps, {}> {
    public render() {

        const tagLinks = this.props.tags.map((tag, index) => {
            return (
                <li key={index} className="nav-item">
                    <Link
                        key={tag}
                        className="nav-link pr-0 pt-0 pb-0"
                        to={tagLink(tag)}
                    >{tag}</Link>
                </li>
            );
        });
        return <PageRow className="tags" right={
            <div className="d-flex flex-row align-items-center">
                <i className="fa fa-tag" aria-hidden="true"/>
                <div>
                    <ul className="nav">
                        {tagLinks}
                    </ul>
                </div>
            </div>
        }/>;
    }
}
