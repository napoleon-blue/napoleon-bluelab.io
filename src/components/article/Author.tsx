/**
 * Created by tottokotkd on 2017/07/14.
 */

import * as React from "react";
import PageRow from "../Row";

export interface IProps {
    name: string;
    bio: string;
    icon?: {
        src: string;
        srcSet: string;
    };
}

export default class extends React.Component<IProps, {}> {
    public render() {
        const {name, bio, icon} = this.props;
        const img = icon ? <img className="icon-image" src={icon.src} srcSet={icon.srcSet} alt={name}/> : null;
        return <PageRow className="author" left={img}
            right={[
                <h2 key={1}>author: <span className="font-weight-bold">{name}</span></h2>,
                <p key={2}>{bio}</p>,
            ]}
        />;

    }
}
