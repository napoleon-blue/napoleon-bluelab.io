/**
 * Created by tottokotkd on 2017/07/14.
 */

import * as moment from "moment-timezone";
import * as React from "react";

import PageRow from "../Row";

export interface IProps {
    createdDate: string;
    updatedDate?: string;
}

export default class extends React.Component<IProps, {}> {

    public render() {
        const {updatedDate, createdDate} = this.props;
        const create = moment(createdDate).tz("Asia/Tokyo").format("YYYY-MM-DD HH:mm");
        const update = updatedDate
            ? moment(updatedDate).tz("Asia/Tokyo").format("YYYY-MM-DD HH:mm")
            : null;

        const timestamp = (updatedDate && create !== update)
            ? <div>{create} <span>(updated: {update})</span></div>
            : <div>{create}</div>;
        return <PageRow className="timestamp" right={
            <div className="d-flex flex-row align-items-center">
                <i className="fa fa-calendar" aria-hidden="true"/>
                <div className="pl-3">
                    {timestamp}
                </div>
            </div>
        }/>;
    }
}
