const fs = require('fs');
const {inputRequired} = require('./utils');
const moment = require('moment-timezone');

const authors = JSON.parse(fs.readFileSync('./data/author.json'));

module.exports = plop => {
    plop.setGenerator('markdown post', {
        prompts: [
            {
                type: 'list',
                name: 'type',
                message: 'The type of post?',
                choices: ['blog', 'news']
            },
            {
                type: 'list',
                name: 'author',
                message: 'The author of blog post?',
                choices: authors.map(author => ({name: author.id, value: author.id}))
            },
            {
                type: 'confirm',
                name: 'draft',
                message: 'It\'s a draft?'
            }
        ],
        actions: data => {
            // Get current date
            const m = moment().tz('Asia/Tokyo');
            data.createdDate = m.tz('Asia/Tokyo').format();
            data.stamp = m.format("YYYY-MM-DD_HHmmssSSS");

            return [
                {
                    type: 'add',
                    path: '../data/{{type}}/{{stamp}}/index.md',
                    templateFile: 'templates/post-md.template'
                }
            ];
        }
    });
};
